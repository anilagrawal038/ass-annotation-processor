//-----------------------------------------------------------------------------------------------------------
//					ORGANIZATION NAME
//Group							: Common - Project
//Product / Project				: ass-annotation-processor
//Module						: ass-annotation-processor
//Package Name					: com.san.common.util
//File Name						: AnnotationProcessorUtil.java
//Author						: anil
//Contact						: anilagrawal038@gmail.com
//Date written (DD MMM YYYY)	: 13-May-2017 4:17:32 PM
//Description					:  
//-----------------------------------------------------------------------------------------------------------
//					CHANGE HISTORY
//-----------------------------------------------------------------------------------------------------------
//Date			Change By		Modified Function 			Change Description (Bug No. (If Any))
//(DD MMM YYYY)
//13-May-2017   	anil			N/A							File Created
//-----------------------------------------------------------------------------------------------------------

package com.san.common.util;

import java.util.Map.Entry;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

public class AnnotationProcessorUtil {

	public static AnnotationMirror findAnnotation(TypeElement typeElement, String annotation) {
		AnnotationMirror mirror = null;
		for (AnnotationMirror annotationMirror : typeElement.getAnnotationMirrors()) {
			if (annotationMirror.getAnnotationType().toString().equals(annotation)) {
				mirror = annotationMirror;
				break;
			}
		}
		return mirror;
	}

	public static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key) {
		for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror.getElementValues().entrySet()) {
			if (entry.getKey().getSimpleName().toString().equals(key)) {
				return entry.getValue();
			}
		}
		return null;
	}

	public static TypeElement asTypeElement(ProcessingEnvironment processingEnv, TypeMirror typeMirror) {
		Types TypeUtils = processingEnv.getTypeUtils();
		return (TypeElement) TypeUtils.asElement(typeMirror);
	}

}
