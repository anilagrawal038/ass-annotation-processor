//-----------------------------------------------------------------------------------------------------------
//					ORGANIZATION NAME
//Group							: Common - Project
//Product / Project				: ass-annotation-processor
//Module						: ass-annotation-processor
//Package Name					: com.san.common.annotation
//File Name						: MandateEntityFieldsInHistory.java
//Author						: anil
//Contact						: anilagrawal038@gmail.com
//Date written (DD MMM YYYY)	: 13-May-2017 11:55:40 AM
//Description					:  
//-----------------------------------------------------------------------------------------------------------
//					CHANGE HISTORY
//-----------------------------------------------------------------------------------------------------------
//Date			Change By		Modified Function 			Change Description (Bug No. (If Any))
//(DD MMM YYYY)
//13-May-2017   	anil			N/A							File Created
//-----------------------------------------------------------------------------------------------------------

package com.san.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface MandateEntityFieldsInHistory {
	Class<?> entityType();
}
