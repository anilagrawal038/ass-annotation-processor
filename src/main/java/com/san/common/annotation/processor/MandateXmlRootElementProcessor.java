//-----------------------------------------------------------------------------------------------------------
//					ORGANIZATION NAME
//Group							: Common - Project
//Product / Project				: ass-common
//Module						: ass-common
//Package Name					: com.san.common.annotation.processor
//File Name						: MandateXmlRootElementProcessor.java
//Author						: anil
//Contact						: anilagrawal038@gmail.com
//Date written (DD MMM YYYY)	: 27-Mar-2017 1:11:41 AM
//Description					:  
//-----------------------------------------------------------------------------------------------------------
//					CHANGE HISTORY
//-----------------------------------------------------------------------------------------------------------
//Date			Change By		Modified Function 			Change Description (Bug No. (If Any))
//(DD MMM YYYY)
//27-Mar-2017   	anil			N/A							File Created
//-----------------------------------------------------------------------------------------------------------

package com.san.common.annotation.processor;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.xml.bind.annotation.XmlRootElement;

import com.san.common.annotation.MandateXmlRootElement;

@SuppressWarnings(value = { "restriction" })
@SupportedAnnotationTypes("com.san.common.annotation.MandateXmlRootElement")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MandateXmlRootElementProcessor extends AbstractProcessor {

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		for (final Element element : roundEnv.getElementsAnnotatedWith(MandateXmlRootElement.class)) {
			if (element instanceof TypeElement) {
				final TypeElement typeElement = (TypeElement) element;
				XmlRootElement xmlRootElement = typeElement.getAnnotation(XmlRootElement.class);
				if (xmlRootElement == null) {
					processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String.format("Class '%s' should be annotated as @XmlRootElement", typeElement.getSimpleName()),
							element);
				}
			}
		}
		return true;
	}

}
