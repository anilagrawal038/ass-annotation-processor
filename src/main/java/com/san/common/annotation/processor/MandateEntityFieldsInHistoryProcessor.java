//-----------------------------------------------------------------------------------------------------------
//					ORGANIZATION NAME
//Group							: Common - Project
//Product / Project				: ass-annotation-processor
//Module						: ass-annotation-processor
//Package Name					: com.san.common.annotation.processor
//File Name						: MandateEntityFieldsInHistoryProcessor.java
//Author						: anil
//Contact						: anilagrawal038@gmail.com
//Date written (DD MMM YYYY)	: 13-May-2017 12:10:14 PM
//Description					:  
//-----------------------------------------------------------------------------------------------------------
//					CHANGE HISTORY
//-----------------------------------------------------------------------------------------------------------
//Date			Change By		Modified Function 			Change Description (Bug No. (If Any))
//(DD MMM YYYY)
//13-May-2017   	anil			N/A							File Created
//-----------------------------------------------------------------------------------------------------------

package com.san.common.annotation.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

import com.san.common.annotation.MandateEntityFieldsInHistory;
import com.san.common.util.AnnotationProcessorUtil;

@SupportedAnnotationTypes("com.san.common.annotation.MandateEntityFieldsInHistory")
@SupportedSourceVersion(SourceVersion.RELEASE_8)

public class MandateEntityFieldsInHistoryProcessor extends AbstractProcessor {
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		for (final Element element : roundEnv.getElementsAnnotatedWith(MandateEntityFieldsInHistory.class)) {
			if (element instanceof TypeElement) {
				final TypeElement typeElement = (TypeElement) element;
				Map<String, Element> fields = new HashMap<String, Element>();
				typeElement.getEnclosedElements().forEach(attr -> {
					if (attr.getKind() == ElementKind.FIELD) {
						fields.put(attr.getSimpleName().toString(), attr);
					}
				});
				AnnotationMirror mandateEntityFieldsInHistory = AnnotationProcessorUtil.findAnnotation(typeElement, "com.san.common.annotation.MandateEntityFieldsInHistory");
				AnnotationValue baseClass = AnnotationProcessorUtil.getAnnotationValue(mandateEntityFieldsInHistory, "entityType");
				TypeMirror baseTypeMirror = (TypeMirror) baseClass.getValue();
				TypeElement baseTypeElement = AnnotationProcessorUtil.asTypeElement(this.processingEnv, baseTypeMirror);
				baseTypeElement.getEnclosedElements().forEach(attr -> {
					if (attr.getKind() == ElementKind.FIELD) {
						if (fields.get(attr.getSimpleName().toString()) == null) {
							processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
									String.format("Field '%s' is missing in Class '%s'", attr.getSimpleName().toString(), typeElement.getSimpleName()), element);
						}
					}
				});
			}
		}
		return true;
	}

}
